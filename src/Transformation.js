const _ = require('lodash')
const Operation = require('./Operation')
const KoaContext = require('./Context').KoaContext
const privates = new WeakMap()
const debug = require('debug')('krm:api')

/**
 * An Transformation is a Promise-based operation that accepts an Operation and a Express
 * request and returns a new Operation.
 *
 * Transformations can be converted to Express middleware that automatically handle serializing
 * Operations to and from the Express request.
 */
class Transformation {
  /**
   * @param {function(Operation, module:express.Request): Promise<Operation>} transformation
   */
  constructor (transformation) {
    privates.set(this, transformation)
  }

  /**
   * Given an initial Operation (to grab options from), returns middleware that does the
   * following:
   *  - Deserialize the current Operation state from the request
   *  - Run the transformation (using the request)
   *  - Serialize the resultant Operation back to the request
   *
   * This middleware is "atomic" in the sense that the request won't be mutated (if the
   * operation is well-behaved) until the operation is finished running.
   *
   * @param {Operation} initialState - initial KRM operation state - just needs to have
   *   "options" set
   * @return {function(*=, *=, *=)}
   */
  getMiddleware (initialState) {
    const transformation = privates.get(this)

    return function transform (ctx, next) {
      debug('%s transformation %s', ctx.state._krmReqId, transformation.name ? transformation.name : 'anonymous')
      let universalCtx = new KoaContext(ctx)
      const currentState = Operation.deserializeRequest(universalCtx)

      return transformation(currentState, universalCtx)
        .then((resultState) => {
          // Add the result to the request object for the next middleware in the stack
          _.merge(universalCtx.krmHost, resultState.serializeToRequest())
          return next()
        })
        .then((resp) => {
          debug('%s transformation %s response', ctx.state._krmReqId, transformation.name ? transformation.name : 'anonymous')
          return Promise.resolve(resp)
        }, (err) => {
          debug('%s transformation %s error response', ctx.state._krmReqId, transformation.name ? transformation.name : 'anonymous')
          return Promise.reject(err)
        })
    }
  }
}

/**
 * An APIOperation is a Transformation that isn't able to operate on the Express request.
 * Since APIOperations are completely decoupled from Express/Restify, they can be used
 * in other applications.
 */
class APIOperation extends Transformation {
  /**
   * @param {function(Operation): Promise<Operation>} operation
   */
  constructor (operation) {
    // Make sure the operation gets called with only the current state
    super(_.unary(operation))
  }

  get operation () {
    return privates.get(this)
  }
}

module.exports = { Transformation, APIOperation }
