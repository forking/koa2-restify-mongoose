const isDistinctExcluded = require('./../shared').isDistinctExcluded
const _ = require('lodash')
const debug = require('debug')('krm:ops')

const APIOperation = require('../../Transformation').APIOperation
const applyQueryToContext = require('../applyQueryToContext')

/**
 * Get all of the items matching a query inside some consumer-provided context.
 *
 * Note: Sets the total count of the operation if the totalCountHeader option is set.
 *
 * @param {Operation} state
 * @param {Object} req
 * @return {Promise<Operation>}
 */
function doGetItems (state) {
  // If distinct is excluded, there won't be anything to return.
  if (isDistinctExcluded(state)) {
    return Promise.resolve(
      state.set('result', []).set('statusCode', 200)
    )
  }

  return applyQueryToContext(state.options, state.context.find(), state.query)
    .then(items => {
      // Find the items for all configurations, and set the status code
      return state.set('result', items).set('statusCode', 200)
    })
    .then(stateWithResult => {
      // If totalCountHeader is set and distinct isn't set, also get the total count
      if (stateWithResult.options.totalCountHeader && !stateWithResult.query.distinct) {
        return getTotalCountHeader(state)
          .then(count => {
            debug('getItems totalCount=%s', count)
            return stateWithResult.set('totalCount', count)
          })
      } else {
        debug('getItems')
        return stateWithResult
      }
    }, err => {
      return Promise.reject(err)
    })
}

/**
 * Get the total count of items matching a query inside some consumer-provided context.
 *
 * Note: sets skip = 0 and limit = 0 for the query in state.
 *
 * @param {Operation} state
 * @param {Object} req
 * @return {Promise<Number>}
 */
function getTotalCountHeader (state) {
  const noSkipOrLimit = _.assign({},
    state.query,
    {
      skip: 0,
      limit: 0
    }
  )

  return applyQueryToContext(state.options, state.context.count(), noSkipOrLimit)
}

module.exports = new APIOperation(doGetItems)
