'use strict'

const debug = require('debug')('krm:koa')
const compose = require('koa-compose')
const getPostMiddlewareForMethod = require('../api/shared').getPostMiddlewareForMethod

module.exports = function (options, excludedMap) {
  function addMiddleware (arr, value) {
    if (value) {
      if (Array.isArray(value)) {
        arr = arr.concat(value)
      } else if (typeof value === 'function') {
        arr.push(value)
      }
    }
  }

  function outputMiddleware (ctx, next) {
    if (ctx.state.krm.result && options.filter) {
      let opts = {
        access: ctx.state._krm.access,
        excludedMap: excludedMap,
        populate: ctx.state._krm.queryOptions
          ? ctx.state._krm.queryOptions.populate
          : null
      }

      ctx.state.krm.result = options.filter.filterObject(ctx.state.krm.result, opts)
    }

    if (options.totalCountHeader && ctx.state.krm.totalCount) {
      const headerName = (typeof options.totalCountHeader === 'string')
        ? options.totalCountHeader
        : 'X-Total-Count'
      ctx.set(headerName, ctx.state.krm.totalCount)
    }

    return options.outputFn(ctx)
  }

  return function prepareOutput (ctx, next) {
    debug(ctx.state._krmReqId + ' prepareOutput')
    let middleware = []
    addMiddleware(middleware, getPostMiddlewareForMethod(options, ctx.method, ctx.state.krm.statusCode))
    middleware.push(outputMiddleware)
    addMiddleware(middleware, options.postProcess)
    return compose(middleware)(ctx, next)
      .then(resp => Promise.resolve(resp))
  }
}
