const http = require('http')

module.exports = function (options) {
  return function (req, res, next) {
    return function (err) {
      req.krm = req.krm || {}

      if (err.message === http.STATUS_CODES[404] || (req.params.id && err.path === options.idProperty && err.name === 'CastError')) {
        req.krm.statusCode = 404
      } else {
        req.krm.statusCode = req.krm.statusCode && req.krm.statusCode >= 400 ? req.krm.statusCode : 400
      }

      options.onError(err, req, res, next)
    }
  }
}
