/**
 * @module './Operation'
 */

const ImmutableRecord = require('immutable-record')
const _ = require('lodash')
const Model = require('mongoose').Model

/**
 * Underlying record for the Operation class
 */
const OperationRecord = ImmutableRecord(
  /** @lends Operation */
  {
    /**
     * Query representing the document(s) we want to operate on
     * @alias module:Operation#context
     * @type {function|Object}
     */
    context: {
      type: ctx => _.isFunction(ctx) || _.isObject(ctx)
    },

    /**
     * The request body, possibly transformed in preparation for the
     * operation.
     * @alias module:Operation#body
     * @type {*}
     */
    body: {},

    /**
     * The permissions granted to whoever is doing the operation
     * @alias module:Operation#accessLevel
     * @type {String}
     */
    accessLevel: {
      type: 'string'
    },

    /**
     * The HTTP status code of the operation, if finished
     * @alias module:Operation#statusCode
     * @type {Number}
     */
    statusCode: {
      type: 'number'
    },

    /**
     * Total count of documents in the operation, if finished
     * @alias module:Operation#totalCount
     * @type {Number}
     */
    totalCount: {
      type: 'number'
    },

    /**
     * The result of the operation, if applicable
     * @alias module:Operation#result
     * @type {Array|Object}
     */
    result: {},

    /**
     * If the operation operates on a single document, this gets set
     * @alias module:Operation#document
     * @type {Object}
     */
    document: {
      type: 'object'
    },

    /**
     * Object that represents the MongoDB query for the operation
     * @alias module:Operation#query
     * @type {Object}
     */
    query: {
      type: 'object'
    },

    /**
     * Top-level KRM options for the operation.
     * @alias module:Operation#options
     * @type {Object}
     */
    options: {
      type: 'object',
      required: true
    },

    /**
     * The mongoose Model we're operating on
     * @alias module:Operation#model
     * @type {mongoose.Model}
     */
    model: {
      type: isModel,
      required: true
    },

    /**
     * Descendant keys to filter out
     * @alias module:Operation#excludedMap
     * @type {Object}
     */
    excludedMap: {
      type: 'object',
      required: true
    }
  },
  'Operation'
)

/**
 * An immutable data structure that represents an in-progress KRM operation.
 * @alias module:Operation
 * @class
 */
class Operation extends OperationRecord {
  /**
   * Return an object that can be stored on an Express request object to persist Operation
   * state in between middleware.
   *
   * @alias module:Operation#serializeToRequest
   * @return {Object}
   */
  serializeToRequest () {
    return {
      krm: {
        model: this.model,
        statusCode: this.statusCode,
        totalCount: this.totalCount,
        result: this.result,
        document: this.document
      },

      _krm: {
        access: this.accessLevel,

        queryOptions: this.query,
        context: this.context,
        body: this.body,
        options: this.options,
        excludedMap: this.excludedMap
      }
    }
  }
}

/**
 * Given an Express request, deserializes an Operation from it.
 *
 * @alias module:Operation.deserializeRequest
 *
 * @param {Object} req - the Express request or Koa2 ctx object
 * @return {Operation}
 */
Operation.deserializeRequest = function (ctx) {
  const pubKrm = ctx.krm || {}
  const privKrm = ctx.privateKrm || {}
  return new Operation(
    _.omitBy({
      model: pubKrm.model,
      statusCode: pubKrm.statusCode,
      totalCount: pubKrm.totalCount,
      result: pubKrm.result,
      document: pubKrm.document,

      accessLevel: privKrm.access,

      query: privKrm.queryOptions,
      context: privKrm.context,
      body: privKrm.body,
      options: privKrm.options,
      excludedMap: privKrm.excludedMap
    }, _.isUndefined)
  )
}

/**
 * Initialize a new Operation with a model, options, and an excludedMap.
 * All parameters are required.
 *
 * @alias module:Operation.initialize
 *
 * @param {Object} model - The mongoose Model we're operating on
 * @param {Object} options - Consumer-specified options
 * @param {Object} excludedMap - Descendant keys to filter out
 *
 * @return {Operation}
 */
Operation.initialize = function (model, options, excludedMap) {
  return new Operation({
    model, options, excludedMap
  })
}

/**
 * Returns true if the argument is a mongoose Model.
 * @param {Object} model
 * @return {boolean}
 */
function isModel (model) {
  return Object.getPrototypeOf(model) === Model
}

module.exports = Operation
