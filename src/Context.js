'use strict'

// STILL WORKING ON THIS ...

class Context {
  constructor (ctx) {
    this._ctx = ctx     // express = { request: req, response: res }, koa = ctx
  }

  get requestBody () {
    return this._ctx.request.body
  }

  // Hack needed by filterRequestBody
  set requestBody (val) {
    this._ctx.request.body = val
  }

  set responseBody (val) {
    this._ctx.response.body = val
  }

  // set reqId (val) {
  //   this.privateKrm.reqId = val
  // }
  //
  // get reqId () {
  //   return this.privateKrm.reqId
  // }
}

class KoaContext extends Context {
  get krm () {
    return this._ctx.state.krm
  }

  get privateKrm () {
    return this._ctx.state._krm
  }

  get krmHost () {
    return this._ctx.state
  }

  get params () {
    return this._ctx.params
  }
}

module.exports = {
  KoaContext: KoaContext
}
