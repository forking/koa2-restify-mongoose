module.exports = function (instance) {
  const getItems = require('./api/db/getItems').getMiddleware(instance)
  const getCount = require('./api/db/getCount').getMiddleware(instance)
  const getShallow = require('./api/db/getShallow').getMiddleware(instance)
  const deleteItems = require('./api/db/deleteItems').getMiddleware(instance)
  const getItem = require('./api/db/getItem').getMiddleware(instance)
  const deleteItem = require('./api/db/deleteItem').getMiddleware(instance)
  const createObject = require('./api/db/createObject').getMiddleware(instance)
  const modifyObject = require('./api/db/modifyObject').getMiddleware(instance)

  return { getItems, getCount, getItem, getShallow, createObject, modifyObject, deleteItems, deleteItem }
}
